/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)
{
    setSize (500, 400);
    
    bpmSlider.setSliderStyle(Slider::LinearHorizontal);
    bpmSlider.setRange(20, 300);
    addAndMakeVisible(bpmSlider);
    bpmSlider.addListener(this);
    
    counterButton.setButtonText("Counter");
    addAndMakeVisible(counterButton);
    counterButton.addListener(this);
    counter.setListener(this);
    
    counter.stopCounter();
    buttonState = false;
    //buttonClicked (&counterButton);
    counter.setBPM(bpmSlider.getValue());
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    counterButton.setBounds(10, 10, 100, 100);
    bpmSlider.setBounds(10, 120, 200, 20);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
void MainComponent::buttonClicked(Button* button)
{
    buttonState = !buttonState;
    if (buttonState == true)
    {
        counter.startCounter();
    }
    else if (buttonState == false)
    {
        counter.stopCounter();
    }
    
    //counterButton.setToggleState(isThreadRunning(), dontSendNotification);

}
void MainComponent::sliderValueChanged(Slider* slider)
{
    counter.setBPM(bpmSlider.getValue());
}
void MainComponent::counterChanged(const unsigned int counterValue)
{
    std::cout << counterValue << "\n";
    audio.beep();
}
