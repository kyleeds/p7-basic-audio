//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 10/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread
{
public:
    /** Constructor */
    Counter();
    
    /** Destructor */
    ~Counter();
    
    void startCounter();
    void stopCounter();
    int nextCounter();
    void setBPM(int newBPM);
    void run() override;
    
    
    /**Class for counter listeners to inherit */
    class Listener
    {
        public:
        /** Destructor. */
        virtual ~Listener() {}
        
    
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    void setListener(Listener* newListener);
private:
    uint32 counter;
    Listener* listener;
    Atomic<float> bpm;
    
};


#endif /* Counter_hpp */
