/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message_)
{
    DBG("Note Number:" << message_.getNoteNumber());
    if (message_.isNoteOn())
    {
        newFrequency.set (MidiMessage::getMidiNoteInHertz (message_.getNoteNumber()));
        gainValue.set (message_.getFloatVelocity());
    }
    else if (message_.isNoteOff())
    {
        newFrequency.set (0.f);
        gainValue.set (0.f);
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    osc.setFrequency (newFrequency.get());
    
    while(numSamples--)
    {
        const float sample = osc.getSample();
        *outL = sample * gainValue.get();
        *outR = sample * gainValue.get();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    osc.setSampleRate (device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}

void Audio::beep()
{
    uint32 time = Time::getMillisecondCounter();
    newFrequency.set(440);
    gainValue.set(1.f);
    Time::waitForMillisecondCounter(time + 100);
    gainValue.set(0.f);
}