//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 03/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator 
{
public:
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    float getSample();
    void setFrequency(int newFrequency);
    void setAmplitude(float newAmplitude);
    void setSampleRate (float newSampleRate);
    
private:
    float frequency;
    float phasePosition;
    float sampleRate;
    float gainValue;
    float phaseIncrement;
    
};

#endif /* SinOscillator_hpp */
