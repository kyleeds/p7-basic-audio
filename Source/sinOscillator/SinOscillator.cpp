//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 03/11/2016.
//
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    frequency = 0.f;
    phasePosition = 0.f;
    sampleRate = 44100;
    gainValue = 1.f;
    phaseIncrement = 0.f;
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    if (phasePosition <= twoPi)
    {
        phasePosition += phaseIncrement;
    }
    else if (phasePosition > twoPi)
    {
        phasePosition -= twoPi;
    }
    
    return sinf (phasePosition) * gainValue;
}

void SinOscillator::setFrequency(int newFrequency)
{
    frequency = newFrequency;
    phaseIncrement = (2 * M_PI * frequency) / sampleRate;
}

void SinOscillator::setAmplitude(float newAmplitude)
{
    gainValue = newAmplitude;
}

void SinOscillator::setSampleRate(float newSampleRate)
{
    sampleRate = newSampleRate;
}